module apiserver

go 1.16

require (
	github.com/deckarep/golang-set v1.8.0
	github.com/elastic/go-elasticsearch/v6 v6.8.10
	github.com/gin-gonic/gin v1.7.7
	gopkg.in/yaml.v2 v2.4.0
)
